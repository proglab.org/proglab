---
layout: default
title: Vejledninger
permalink: /vejledninger/
---

{% for vejledning in site.vejledninger %}
<h2>
<a href="{{ vejledning.url | prepend: site.baseurl }}">
  <h2>{{ vejledning.title }}</h2>
</a>
</h2>
<p class="post-excerpt">{{ vejledning.description | truncate: 160 }}</p>

{% endfor %}