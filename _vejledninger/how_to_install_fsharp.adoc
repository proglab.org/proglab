---
title: "Sådan installerer du FSharp og Xelmish på alle typer OS"
date: 2020-02-02T20:37:39+01:00
published: false
draft: true
---
[big]#Det her er en guide til hvordan man installerer FSharp og Xelmish, som er det nye programmeringsprog som vi vil prøve om er godt nok til at erstatte PyGame.#




== Mac

=== FSharp

----
brew install mono
brew cask install dotnet-sdk
----

=== nuget

Download og tilføj det til din `PATH`.

----
sudo curl -o /usr/local/bin/nuget.exe https://dist.nuget.org/win-x86-commandline/latest/nuget.exe
----

Lav et alias til nuget:

----
alias nuget="mono /usr/local/bin/nuget.exe"
----


== Windows

=== FSharp

Vælg "Option 4" på https://fsharp.org/use/windows/, dvs:

Installér .NET Core SDK og tilføj `C:\Program Files\dotnet` til din `PATH`.


=== nuget

Gå til https://docs.microsoft.com/en-us/nuget/install-nuget-client-tools og download `nuget.exe`.

Placér `nuget.exe` i `C:\tools` og tilføj denne folder til din `PATH`.

== Linux

== Chromebook

== Xelmish

Start en terminal og skriv:

----
nuget install xelmish
----

Lav en klon af Xelmish og prøv eksemplerne af.

----
git clone https://github.com/ChrisPritchard/Xelmish.git
----

Eksemplerne ligger som underfoldere til folderen `samples`.

Inden man kan køre eksemplerne skal man ændre `.fsproj` filen, så `TargetFramework` har den rigtige version. Brug kun major og minor revision, f.eks., 3.1.

På en kommandolinje i folderen med eksemplet kan man så køre programmet vha:

----
dotnet run
----
