import turtle


def go_north():
  turtle.setheading(90)
  if turtle.ycor() < 300:
    turtle.forward(50)
    go_north()
