---
title: "Fraktaler med Turtle"
categories: Vejledninger
---
= Fraktaler med Turtle

:imagesdir: images
:doctype: article
////
:toc:
:toclevels: 2
////
:numbered:
:icons: font

:source-highlighter: rouge

:source-language: python

Fraktaler i denne henseende er figurer, hvor den samme struktur går igen på alle størrelsesniveauer.

Vi skal prøve, om vi kan tegne nogle af de mest kendte fraktaler med Turtle.


== Fraktaltræ

Kaldes også et  link:https://en.wikipedia.org/wiki/Pythagoras_tree_(fractal)[Pythagorastræ].

Idéen er, at man tegner en stamme og så laver man forgreninger til venstre og højre, hvor man tegner et mindre træ, indtil man når til et nulte-ordenstræ, som bare er en streg.

Her er et eksempel:

image::pythagoras_tree.jpg[]

Det er et femte-ordenstræ.

Da figuren er rekursiv, skal vi nok bruge en rekursiv funktion, dvs en funktion der kalder sig selv.

Når man laver rekursive funktioner, så skal man altid sørge for at man gør ens parametre mindre… ellers går programmet i uendelig løkke. I vores tilfælde skal vi stoppe når vi når til nulte-ordenstræ.

Lav en fil med navnet `fractal_tree.py` og lav følgende funktion:

[source, python]
----
def tree(n, length, angle=45):
  % your code here!
----

Hint:

- gem turtles position og retning inden du laver venstre træ.

Når du har fået det til at virke, så prøv at lege med:

- hvor meget mindre skal længden være for hvert niveau.
- lav tilfældige vinkler og længder.
- stop når længden er for lille.
- brug flere turtles til at tegne træet.

